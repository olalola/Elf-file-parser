#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <elf.h>
#include <sys/types.h>
#include <sys/user.h>
#include <sys/stat.h>
#include <sys/ptrace.h>
#include <sys/mman.h>


typedef struct elfFile
{
    uint8_t *mem;
    Elf32_Ehdr *ehdr;
    Elf32_Phdr *phdr;
    Elf32_Shdr *shdr;

} elfFile_t;

typedef struct segment
{
    uint32_t *task;
    Elf32_Off *shift;
    Elf32_Addr *virtualAddress;
    Elf32_Addr *physicalAddress;
    uint32_t *segmentSizeInFile;
    uint32_t *segmentSizeInMemory;
    uint32_t *flag;
    uint32_t *alignmentSeg;
} sgt;

int main(int argc, char **argv, char **envp)
{
    int fd;
    struct stat st;
    char *executableName;

    elfFile_t h;


    if ((executableName = strdup(argv[1])) == NULL)
    {
        perror("strdup");
    }

    if((fd = open(argv[2], O_RDONLY)) < 0)
    {
        perror("open");
    }

    if (fstat(fd, &st) < 0)
    {
        perror("fstat");
    }

    h.mem = mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);

    if (h.mem == MAP_FAILED)
    {
        perror("mmap");
    }


    h.ehdr = (Elf32_Ehdr *) h.mem;
    h.phdr = (Elf32_Phdr *) (h.mem + h.ehdr->e_phoff);
    //h.shdr = (Elf32_Shdr *) (h.mem + h.ehdr->e_shoff);



    //elf file headers
    if(strcmp(argv[1], "--elf-file-header") == 0)
    {
        printf("En-tête ELF:\n");
        switch(h.ehdr->e_version)
        {
            case EV_NONE:
                printf("  version: 0x%x(non valable)\n", h.ehdr->e_version);
                break;
            case EV_CURRENT:
                printf("  version: 0x%x(actuelle)\n", h.ehdr->e_version);
                break;
        }


        switch (h.ehdr->e_machine)/*je n'ai pas tout écrit sinon ça devriendrait vit echiant*/
        {
            case EM_NONE:
                printf("  machine inconnue\n");
                break;
            case EM_M32:
                printf("  machine AT&T WE 32100\n");
                break;
            case EM_SPARC:
                printf("  machine Sun Microsystems SPARC\n");
                break;
            case EM_386:
                printf("  machine Intel 80386\n");
                break;
            case EM_68K:
                printf("  machine Intel Motorola 68000\n");
                break;
            /*case etc...*/
        }

        switch (h.ehdr->e_type)
        {
            case ET_NONE:
                printf("  fichier illisible\n");
                break;
            case ET_REL:
                printf("  fichier relocalisable\n");
                break;
            case ET_EXEC:
                printf("  fichier executable\n");
                break;
            case ET_DYN:
                printf("  fichier d'objets partagés\n");
                break;
            case ET_CORE:
                printf("  fichier de rapport de plantage\n");
                break;
            }

    printf("  point d'entré: 0x%x\n", h.ehdr->e_entry);
    }

    else if(strcmp(argv[1], "--elf-program-header") == 0)
    {
        printf("en-têtes programme:\n");
        for (int i = 0; i < h.ehdr->e_phnum; i ++)
        {
            //sgt tmpSegment = {task = h.phdr[i].p_type; shift = h.phdr.p_offset; virtualAddress = h.phdr[i].p_vaddr; physicalAddress = h.phdr[i].p_vaddr; segmentSizeInFile = h.phdr[i].p_filesz; segmentSizeInMemory = h.phdr[i].p_memsz, flag = h.phdr[i].p_flags; alignmentSeg = h.phdr[i].p_align};
            sgt tmpSegment;
            tmpSegment.task = h.phdr[i].p_type;
            tmpSegment.shift = h.phdr[i].p_offset;
            tmpSegment.virtualAddress = h.phdr[i].p_vaddr;
            tmpSegment.physicalAddress = h.phdr[i].p_vaddr;
            tmpSegment.segmentSizeInFile = h.phdr[i].p_filesz;
            tmpSegment.segmentSizeInMemory = h.phdr[i].p_memsz;
            tmpSegment.flag = h.phdr[i].p_flags;
            tmpSegment.alignmentSeg = h.phdr[i].p_align;

            switch(h.phdr[i].p_type)
            {
                printf("TYPE-OFFSET-ADDRESSE VIRTUELLE-ADRESSE PHYSIQUE-TAILLE SEGMENT DANS LE FICHIER-TAILLE SEGMENT EN MEMOIRE-DRAPEAU-ALIGNEMENT\n");
                case PT_LOAD:

                    if(h.phdr[i].p_offset == 0)
                    {
                        printf("  début de PT_LOAD: 0x%x-0x%x-0x%-x0x%x-0x%x-0x%x-0x%x\n", tmpSegment.shift, tmpSegment.virtualAddress, tmpSegment.physicalAddress, tmpSegment.segmentSizeInFile, tmpSegment.segmentSizeInMemory, tmpSegment.flag, tmpSegment.alignmentSeg);
                    }
                    else
                    {
                        printf("  fin de PT_LOAD: 0x%x-0x%x-0x%-x0x%x-0x%x-0x%x-0x%x\n", tmpSegment.shift, tmpSegment.virtualAddress, tmpSegment.physicalAddress, tmpSegment.segmentSizeInFile, tmpSegment.segmentSizeInMemory, tmpSegment.flag, tmpSegment.alignmentSeg);
                    }
                    break;
                case PT_DYNAMIC:
                    printf("  segment PT_DYNAMIC: 0x%x-0x%x-0x%-x0x%x-0x%x-0x%x-0x%x\n", tmpSegment.shift, tmpSegment.virtualAddress, tmpSegment.physicalAddress, tmpSegment.segmentSizeInFile, tmpSegment.segmentSizeInMemory, tmpSegment.flag, tmpSegment.alignmentSeg);
                    break;
                case PT_NOTE:
                    printf("  segment PT_DYNAMIC: 0x%x-0x%x-0x%-x0x%x-0x%x-0x%x-0x%x\n", tmpSegment.shift, tmpSegment.virtualAddress, tmpSegment.physicalAddress, tmpSegment.segmentSizeInFile, tmpSegment.segmentSizeInMemory, tmpSegment.flag, tmpSegment.alignmentSeg);
                    break;
                case PT_INTERP:
                    printf("  segment PT_DYNAMIC: 0x%x-0x%x-0x%-x0x%x-0x%x-0x%x-0x%x\n", tmpSegment.shift, tmpSegment.virtualAddress, tmpSegment.physicalAddress, tmpSegment.segmentSizeInFile, tmpSegment.segmentSizeInMemory, tmpSegment.flag, tmpSegment.alignmentSeg);
                    break;
                case PT_PHDR:
                    printf("  segment PT_DYNAMIC: 0x%x-0x%x-0x%-x0x%x-0x%x-0x%x-0x%x\n", tmpSegment.shift, tmpSegment.virtualAddress, tmpSegment.physicalAddress, tmpSegment.segmentSizeInFile, tmpSegment.segmentSizeInMemory, tmpSegment.flag, tmpSegment.alignmentSeg);
                    break;
            }
        }
    }


    //elf program header


    return 0;
}
